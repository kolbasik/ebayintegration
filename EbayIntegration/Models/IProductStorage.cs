﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace EbayIntegration.Models
{
    public interface IProductStorage
    {
        List<Product> RetrieveAllProducts();
        Product RetrieveProductByID(int id);
        List<Product> RetrieveProductByName(string name);
        void UpdateProduct(Product product);
        void DeleteProduct(int id);
    }
}