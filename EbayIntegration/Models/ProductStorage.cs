﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace EbayIntegration.Models
{
    [DataContract]
    public class ProductStorage : IProductStorage
    {
        public ProductStorage()
        {
            ProductCollection=new List<Product>
            {
                new Product{Id = 1,Name = "Name1",Description = "Description1",Image = @"/Content/Image/1.jpg",Price = 110.5M,Discount = 0,DiscountPercent = 0},
                new Product{Id = 2,Name = "Name2",Description = "Description2",Image = @"/Content/Image/2.jpg",Price = 120.5M,Discount = 0,DiscountPercent = 0},
                new Product{Id = 3,Name = "Name3",Description = "Description3",Image = @"/Content/Image/3.jpg",Price = 130.5M,Discount = 0,DiscountPercent = 0},
                new Product{Id = 4,Name = "Name4",Description = "Description4",Image = @"/Content/Image/4.jpg",Price = 140.5M,Discount = 0,DiscountPercent = 0},
                new Product{Id = 5,Name = "Name5",Description = "Description5",Image = @"/Content/Image/5.jpg",Price = 150.5M,Discount = 0,DiscountPercent = 0},
                new Product{Id = 6,Name = "Name6",Description = "Description6",Image = @"/Content/Image/6.jpg",Price = 160.5M,Discount = 0,DiscountPercent = 0},
                new Product{Id = 7,Name = "Name7",Description = "Description7",Image = @"/Content/Image/7.jpg",Price = 170.5M,Discount = 0,DiscountPercent = 0},
                new Product{Id = 8,Name = "Name8",Description = "Description8",Image = @"/Content/Image/8.jpg",Price = 180.5M,Discount = 0,DiscountPercent = 0},
                new Product{Id = 9,Name = "Name9",Description = "Description9",Image = @"/Content/Image/9.jpg",Price = 190.5M,Discount = 0,DiscountPercent = 0}
            };
        }

        [DataMember]
        public List<Product> ProductCollection;

        public List<Product> RetrieveAllProducts()
        {
            return ProductCollection;
        }

        public Product RetrieveProductByID(int id)
        {
            return ProductCollection.FirstOrDefault(x => x.Id == id);
        }

        public List<Product> RetrieveProductByName(string name)
        {
            return ProductCollection.Where(x => x.Name == name).ToList();
        }

        public void UpdateProduct(Product product)
        {
            var updatedProduct=ProductCollection.FirstOrDefault(x => x.Id == product.Id);
            if (updatedProduct != null)
            {
                updatedProduct.Name = product.Name;
                updatedProduct.Price = product.Price;
                updatedProduct.Description = product.Description;
                updatedProduct.Discount = product.Discount;
                updatedProduct.DiscountPercent = product.DiscountPercent;
            }
            else throw new ArgumentNullException("Product", "Could not find specifuc product");
        }

        public void DeleteProduct(int id)
        {
            var deletedProduct = ProductCollection.FirstOrDefault(x => x.Id == id);
            if (deletedProduct != null)
            {
                ProductCollection.Remove(deletedProduct);
            }
            else throw new ArgumentNullException("Product", "Could not find specifuc product");
        }
    }
}