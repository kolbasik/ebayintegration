﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace EbayIntegration.Models
{
    [DataContract]
    public class Product
    {

        private int _discountPercent;
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Image { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public decimal Discount { get; set; }

        [DataMember]
        [Range(0, 100)]
        public int DiscountPercent
        {
            get
            {
                return _discountPercent;
            }
            set
            {
                if (value < 0 || value > 100) throw new ArgumentOutOfRangeException("DiscountPercent", "Field shoud be bettween 0 and 100");
                else _discountPercent = value;
            }
        }

    }
}