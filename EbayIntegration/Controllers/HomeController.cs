﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using EbayIntegration.com.ebay.developer;
using EbayIntegration.Models;

namespace EbayIntegration.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        private ProductStorage _productStorage;

        public HomeController()
        {
            if (_productStorage == null)
                _productStorage = new ProductStorage();
        }

        public ActionResult Index()
        {
            return View(_productStorage.ProductCollection);
        }


        public ActionResult Index1()
        {



            /*var client = new com.ebay.developer.eBayAPIInterfaceService();
            var credentials = new CustomSecurityHeaderType
            {
                Credentials = new UserIdPasswordType
                {
                    AppId = "Homeb7b4e-f2ed-4f3c-bb31-e5642fdd5ad",
                    DevId = "48a7c1e5-8013-4794-917d-ac39f7a23c09",
                    AuthCert = "234867fb-7682-4d3b-b624-365e37810917"
                },
                eBayAuthToken =
                    "AgAAAA**AQAAAA**aAAAAA**uDG4Ug**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GhC5aKog6dj6x9nY+seQ**lngCAA**AAMAAA**jnkYhClsYILBc5DYTPBm+0i9DRuPsdeKem/RnfqXznynx7QI4nTu5xiVv4U21lEhKVZ4NEv1vqqqd/DbEXzJNooJ8z+qAfu93hw/7Z/6fa1o/FS5suBKIhVqsN1n3rrJQxlCrNOZTNTXF6aGSsM2wzNKX0AKXL0zhpjatE9MpVfIRgBVE9U44v/SttwEm77iTKHefjkQwZvD5epQjKW2+duwra9fokkU10S3CYH+/ATTGhgdp854Y2ki/Of2FHBfmxa/Qg8kZMowQ4JGX4gW0Xx7nqGTctNMh+Tkne8ffGb1H3o/RFjGxgZCx0CIvQfFjOzojREgzIC5andQbVZNIB6H2GPCFOJNNkyqvu7lvgp97KDq68IH/wVdrlaoMwR1Owd4m1ABumFU+mN2jnyJQunCjGLk1K04u4E3dTPlVwbIudJaOykXmd3zv7ID75+KH5N+2D9r7VNzGspHem+pumMt6awHnNnLzp6jtHYuFckhdcl8XuYq3u4JZC1mv3ZXotZfzRgBgZPMp2ANMRcGPGok7niXNJFhKTQYBcFrNd9genI9YTo5P9a+tYmLikJY3TU3M9ekV9NfkQliRYPzZ7pwv7r9ATIPEwkf/hH06DyPJDfhxSr4qRQEYDsdKt5qKVLTUV3ONKwcCl+UGvCV2LAHS+HAAYEgrHvVFxwNa/nk2VCZG5+m5A2Cj6/EwdADmv6YNqsA9Bu0tZ599SLp1UPbMG3wHDxHyeqEp8PTqcUd5enVmroC+zY+CIgmqtw2"
            };

            client.Url = "https://api.ebay.com/wsapi";
            var ebayTimeReq = new GeteBayOfficialTimeRequestType { Version = "551" };

            GeteBayOfficialTimeResponseType response = client.GeteBayOfficialTime(ebayTimeReq);*/


            string endpoint = "https://api.ebay.com/wsapi";
            string callName = "GetMyeBayBuying";
            string siteId = "0";
            string appId = "Homeb7b4e-f2ed-4f3c-bb31-e5642fdd5ad";  // use your app ID 
            string devId = "48a7c1e5-8013-4794-917d-ac39f7a23c09";  // use your dev ID 
            string certId = "234867fb-7682-4d3b-b624-365e37810917";  // use your cert ID 
            string version = "437";
            // Build the request URL 
            string requestURL = endpoint + "?callname=" + callName + "&siteid=" + siteId + "&appid=" + appId + "&version=" + version + "&routing=default";
            // Create the service 
            var service = new eBayAPIInterfaceService();
            service.RequesterCredentials = new CustomSecurityHeaderType();
            // service.RequesterCredentials.eBayAuthToken = "AgAAAA**AQAAAA**aAAAAA**uDG4Ug**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GhC5aKog6dj6x9nY+seQ**lngCAA**AAMAAA**jnkYhClsYILBc5DYTPBm+0i9DRuPsdeKem/RnfqXznynx7QI4nTu5xiVv4U21lEhKVZ4NEv1vqqqd/DbEXzJNooJ8z+qAfu93hw/7Z/6fa1o/FS5suBKIhVqsN1n3rrJQxlCrNOZTNTXF6aGSsM2wzNKX0AKXL0zhpjatE9MpVfIRgBVE9U44v/SttwEm77iTKHefjkQwZvD5epQjKW2+duwra9fokkU10S3CYH+/ATTGhgdp854Y2ki/Of2FHBfmxa/Qg8kZMowQ4JGX4gW0Xx7nqGTctNMh+Tkne8ffGb1H3o/RFjGxgZCx0CIvQfFjOzojREgzIC5andQbVZNIB6H2GPCFOJNNkyqvu7lvgp97KDq68IH/wVdrlaoMwR1Owd4m1ABumFU+mN2jnyJQunCjGLk1K04u4E3dTPlVwbIudJaOykXmd3zv7ID75+KH5N+2D9r7VNzGspHem+pumMt6awHnNnLzp6jtHYuFckhdcl8XuYq3u4JZC1mv3ZXotZfzRgBgZPMp2ANMRcGPGok7niXNJFhKTQYBcFrNd9genI9YTo5P9a+tYmLikJY3TU3M9ekV9NfkQliRYPzZ7pwv7r9ATIPEwkf/hH06DyPJDfhxSr4qRQEYDsdKt5qKVLTUV3ONKwcCl+UGvCV2LAHS+HAAYEgrHvVFxwNa/nk2VCZG5+m5A2Cj6/EwdADmv6YNqsA9Bu0tZ599SLp1UPbMG3wHDxHyeqEp8PTqcUd5enVmroC+zY+CIgmqtw2";
            service.RequesterCredentials.Credentials = new UserIdPasswordType();
            service.RequesterCredentials.Credentials.AppId = appId;
            service.RequesterCredentials.Credentials.DevId = devId;
            service.RequesterCredentials.Credentials.AuthCert = certId;
            var respSes = service.GetSessionID(new GetSessionIDRequestType() { RuName = "MyRu" });
            var response = service.GetCategories(new GetCategoriesRequestType());

            return View();
        }
    }
}
